/**
 * jQuery Form Validation
 * Name: jsFormVal
 * Author: Frederick M. Rogers
 * Description:
 * 
 */

(function($) {

    ;(function($,window, document, undefined) {

        var jsFormVal = "jsFormVal",
            defaults = {
                submit: null,
                charCounter: {                     
                    minPasswordLength: 6,
                    phoneNumberLength: 8,
                    maxTextLength: 256,
                },
            };

        function Plugin(element, options) {
            this.$form = $(element); 
            this.formId = $(element).attr('id');
            this.options = $.extend({}, defaults, options);
            
            this.defaults = defaults;
            this.name = jsFormVal;

            this.counterTypes = 'textarea, input[type="password"], input[type="number"]';

            this.formValues = {};
            
            this.init();            
        };       

        Plugin.prototype.init = function() {
            var _this = this;

            // initilize character count
            _this.characterCounter();

            // initilize click event            
            if (this.options.submit !== null) {
                $(this.options.submit).on('click',function() {                
                    _this.validate();
                });
            }            
        };

        Plugin.prototype.validate = function() {
            var _this = this;

            $('#' + this.formId + ' *').filter(':input').each(function() {
                var $this = $(this);
                var inputType = $this.attr('type');   
                var inputName = $this.attr('name');
                var inputValue = $this.val();
                var $thisParent = $this.parents('.form-group');             

                // Test for specific field type
                if (inputType == 'text' ) {
                    // Test if field is empty
                    if (inputValue === '') {
                        // initilize response
                        $thisParent.addClass('has-error');
                    }               
                }

                // Test for valid email
                if (inputType == 'email') {
                    var email = inputValue;
                    var regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!regexEmail.test(email)) {                        
                        $thisParent.addClass('has-error');
                    }
                }

                // Test if phone field is a valid number
                if (inputType == 'number') {
                    var number = inputValue;
                    var regexNumber = /\d/g;
                    if (!regexNumber.test(number) || number.length !== _this.options.charCounter.phoneNumberLength) {
                        $thisParent.addClass('has-error');                        
                    }
                    
                }
                
                // Test for minimum lenght of password
                if (inputType == 'password') {
                    var password = inputValue;
                   
                    if (password.length < _this.options.charCounter.minPasswordLength) {
                        $thisParent.addClass('has-error');                         
                    }
                }

                // Test for maximum length of text
                if ($this.is('textarea')) {
                    var text = inputValue;
                    if (text.length > _this.options.charCounter.maxTextLength || text === '') {
                        $thisParent.addClass('has-error');   
                    }
                }
                $this.on('focus', function(event) {
                    if (event.type == 'focus' && (inputType == 'text' || inputType == 'email')) {
                        $this.on('keyup', function() {
                            if ($this.val() !== '') {
                                $thisParent.removeClass('has-error');
                            }
                        });
                    }
                });
            });
        };

        Plugin.prototype.characterCounter = function() {
            var _this = this;

            if (this.options.charCounter !== null) {                
                $('#' + this.formId + ' *').filter(this.counterTypes).each(function() {
                    var $this = $(this);                    
                    var $thisParent = $this.parents('.form-group');
                    
                    $this.wrap('<div class="input-group"></div>').parent().append('<div class="input-group-addon counter"></div>');

                    $this.on('focus blur', function(event) {

                        var $thisNext = $this.next();

                        if (event.type == 'focus') {                            

                            if ($this.is('input[type="number"]') && $this.val() === '') {
                                $thisNext.text(_this.options.charCounter.phoneNumberLength + " " + "Digits");
                                $thisParent.addClass('has-error');
                            } 
                            else if ($this.is('input[type="password"]') && $this.val() === '') {
                                $thisNext.text("min" + " " + _this.options.charCounter.minPasswordLength + " " + "Chars");
                                $thisParent.addClass('has-error');
                            } 
                            else if ($this.is('textarea') && $this.val() === '') {
                                $thisNext.text("max\n" + _this.options.charCounter.maxTextLength + "\n" + "Chars").css('white-space', 'normal');
                                $thisParent.addClass('has-success');
                            }
                            //$thisParent.addClass('has-error');                            
                        } 
                        else if (event.type == 'blur' && $this.val() === '') {
                            $thisParent.removeClass('has-error has-success');
                        }                    
                    });                    

                    $this.on('keyup', function() {
                        
                        //* Number field                        
                        if ($this.is('input[type="number"]')) {
                            $thisNext.text($this.val().length + " " + "Digits");

                            if ($this.val().length == _this.options.charCounter.phoneNumberLength) {
                                $thisParent.removeClass('has-error');
                                $thisParent.addClass('has-success');
                            } 
                            else {
                                $thisParent.addClass('has-error');
                            }
                        } 

                        //* Password field
                        if ($this.is('input[type="password"]')) {
                            $thisNext.text($this.val().length + " " + "Chars");

                            if ($this.val().length >= _this.options.charCounter.minPasswordLength) {
                                $thisParent.removeClass('has-error');
                                $thisParent.addClass('has-success');    
                            }
                        }

                        //* Texarea
                        if ($this.is('textarea')) {
                            $thisNext.text($this.val().length + " " + "Chars");

                            if($this.val().length > _this.options.charCounter.maxTextLength) {
                                $thisParent.removeClass('has-success');
                                $thisParent.addClass('has-error');
                            } 
                            else {
                                $thisParent.removeClass('has-error');
                                $thisParent.addClass('has-success');
                            }
                        }
                    });

                    
                });
            }

        };

        

        $.fn[jsFormVal] = function(options) {
            return this.each(function() {
                if (!$.data(this, "plugin_" + jsFormVal)) {
                    $.data(this, "plugin_" + jsFormVal, new Plugin(this, options));
                }
            });
        };

    })(jQuery, window, document);

})(jQuery);